package ru.iskra.appazimuthtac;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.Manifest;
import android.content.pm.PackageManager;
import android.app.TimePickerDialog;

import android.widget.DatePicker;
import android.widget.TimePicker;

//import com.basewin.aidl.OnPrinterListener;
//import com.basewin.services.PrinterBinder;
//import com.basewin.services.ServiceManager;
//
import ru.nilsoft.tm.*;

public class MainActivity extends AppCompatActivity {

    String[] data = {"1", "5", "10"};

    /** Интерфейсная библиотека. */
    private TMLib libTM;
    //*Функции ФР

    TMAzimuth tmAZ;

    static Context cnt;

    ProgressDialog progressDialog;
    Handler handler = new Handler();
    String ofd_text = "";

    Integer dd = 0;

    Integer attempts = 0;
    Boolean OFDsendCancel = false;
    boolean isCancelProgress = false;
    String txtMessageProgress = "";
    String item = "1";

    final int TIMEDIPLAY = 3000;
    TextView currentDateTime;
    Calendar dateAndTime= Calendar.getInstance();
    int i_counter;
    String data_limit;
    boolean checkBoxPDF417 = false;

    // установка начальных даты и времени
    private void setInitialDateTime(TextView currentDateTime) {

        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date date = dateAndTime.getTime();
        String datetime = myFormat.format(date);
        currentDateTime.setText(datetime);
        //System.out.println("Current Date Time in give format: " + datetime);

//        currentDateTime.setText(DateUtils.formatDateTime(this,
//                dateAndTime.getTimeInMillis(),
//                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_TIME));
    }

    private void runH(String txt, boolean isCancel) {
        txtMessageProgress = txt;
        isCancelProgress = isCancel;
        handler.post(new Runnable(){
            public void run() {
                startProgressDialog(txtMessageProgress, isCancelProgress);
            }
        });
    }

    private void stopH() {
        handler.post(new Runnable(){
            public void run() {
                stopProgressDialog();
            }
        });
    }

    private void startProgressDialog(String txt, boolean isCancel) {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Выполняется операция");
        }

        if(isCancel == true) {
            progressDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    i_counter = attempts;
                    attempts = 0;
                    OFDsendCancel = true;
                    return;
                }
            });
        }
        else {

        }
        progressDialog.setCancelable(false);
        progressDialog.setMessage(txt);
        progressDialog.show();
    }

    private void stopProgressDialog() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setTitle("Тест AzimithTac 1.15");

        handler = new Handler();

        cnt = this;

        // Комбо бокс ComboBox
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        spinner.setAdapter(adapter);

        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // Получаем выбранный объект
                item = (String)parent.getItemAtPosition(position);
                Toast toast = Toast.makeText(getApplicationContext(), "Выбрали: " + item, Toast.LENGTH_LONG);
                toast.show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        spinner.setOnItemSelectedListener(itemSelectedListener);


    }

    //private volatile static PrinterBinder prn = null;

    /** Запрос прав от пользователя (доступно только в Android 6.0 и выше). */
    public void RequestPermissions() {

        //просим разрешение (доступно только в Android 6.0)
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            requestPermissions(
                    new String[]{
                            Manifest.permission.READ_CALENDAR,
                            Manifest.permission.WRITE_CALENDAR,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.INTERNET,
                            Manifest.permission.WAKE_LOCK,
                            "com.pos.permission.SECURITY",
                            "com.pos.permission.COMMUNICATION",
                            "com.pos.permission.PRINTER",
                            "android.permission.WRITE_EXTERNAL_STORAGE",
                            "android.permission.READ_EXTERNAL_STORAGE"
//                            "com.pos.permission.CARD_READER_MAG",
//                            "com.pos.permission.CARD_READER_ICC",
//                            "com.pos.permission.CARD_READER_PICC"
                    }, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if  ( (requestCode == 0) && ( grantResults.length > 0 ) ) {
            for( int i = 0; i < grantResults.length; i++) {
                if ( grantResults[i] != PackageManager.PERMISSION_GRANTED ) {
                    //return;
                }
            }

            SystemPrint.Init();
            try {
                //создание интерфейса (singletone)
                libTM = TMLib.getInstance();

                libTM.isLogSet(true);

                libTM.TMLibLoadFR();
                libTM.TMLibLoadPrint(getApplicationContext());

                libTM.grayInt = 1200;
                tmAZ = new TMAzimuth(libTM);
            }
            catch (Exception e) {
                Toast.makeText(this, "Exception onStart: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RequestPermissions();
//        try {
//            ServiceManager.getInstence().init(cnt);
//            prn = ServiceManager.getInstence().getPrinter();
//        }catch (Exception eee) {
//
//        }


        if ( Build.VERSION.SDK_INT < Build.VERSION_CODES.M ) {
            SystemPrint.Init();
            try {
                //создание интерфейса (singletone)
                libTM = TMLib.getInstance();

                libTM.TMLibLoadFR();
                libTM.TMLibLoadPrint(getApplicationContext());

                libTM.grayInt = 1200;
                tmAZ = new TMAzimuth(libTM);
            }
            catch (Exception e) {
                Toast.makeText(this, "Exception onStart: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }


    public void pressXReport(View b) {
        try {
            startProgressDialog("Печать: X-Report", false);

            Runnable runnable = new Runnable() {
                public void run() {
                    // Код в потоке
                    libTM.isLogSet(false);
                    int res = tmAZ.xReport();
                    String err = TMError.GetText((byte)res);

                    runH("xReport: " + Integer.toString(res) + " " + err, false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    runH("Ответ: " + libTM.getLastResCMD(), false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    stopH();
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();


        }
        catch (Exception e) {
            Toast.makeText(this, "Exception pressXReport: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void pressOpenShift(View b) {
        try {

            startProgressDialog("Печать: Открытие смены", false);

            Runnable runnable = new Runnable() {
                public void run() {
                    // Код в потоке
                    int res = tmAZ.shiftOpen("Текст Текст Текст");
                    String err = TMError.GetText((byte)res);

                    runH("shiftOpen: " + Integer.toString(res) + " " + err, false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    runH("Ответ: " + libTM.getLastResCMD(), false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    stopH();

                }
            };
            Thread thread = new Thread(runnable);
            thread.start();

        }
        catch (Exception e) {
            Toast.makeText(this, "Exception pressOpenShift: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public void pressCloseShift(View b) {

        try {

            startProgressDialog("Печать: Закрытие смены", false);

            Runnable runnable = new Runnable() {
                public void run() {
                    int res = tmAZ.shiftClose();
                    String err = TMError.GetText((byte)res);

                    runH("shiftClose: " + Integer.toString(res) + " " + err, false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    runH("Ответ: " + libTM.getLastResCMD(), false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    stopH();

                }
            };
            Thread thread = new Thread(runnable);
            thread.start();


        }
        catch (Exception e) {
            Toast.makeText(this, "Exception pressCloseShift: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    /** Класс контролирующий время ожидания ответа. */
    public class PrintTest extends AsyncTask<Integer, Void, Void> {
        /** Обновление текста в потоке активности. */
//        protected void onProgressUpdate(Void... params) {
//            addTextToView(params[0]);
//        }

        /** После останова потока, выполнение в потоке активности. */
        protected void onPostExecute(Void result) {
//            if ( mode == state.WRITE ) FRFNWriteScript(); //сохраняем пакет в ФН, если получен ответ из ОФД
//            sendPack = null;
        }

        /** Метод работающий в потоке. */
        protected Void doInBackground(Integer... params) {
            attempts = params[0];
            Integer time = params[1];
            txtMessageProgress = "Тестирование: ";
            runH(txtMessageProgress, true);

            for(i_counter = 0; i_counter<attempts; i_counter++) {

                try {
                    Date dateNow = new Date();
                    String myStrDate = data_limit;
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = format.parse(myStrDate);
                    if (dateNow.getTime() > date.getTime()) {

                        i_counter = 0;
                        attempts = 0;

                        txtMessageProgress = "Тестирование завершено по истечению времени";
                        runH(txtMessageProgress, false);
                        try { Thread.sleep(1000); }catch (Exception ee){ }

                        stopH();
                        return null;
                    }
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                txtMessageProgress = "Тестирование: " + (i_counter+1) + " из " + attempts;
                runH(txtMessageProgress, true);
                try { Thread.sleep(1000); }catch (Exception ee){ }

                //printFRCommand();
                printTTTest();
//                runOnUiThread(  new Runnable() {
//                    public void run() {
//
//                        printFRCommand();
//                    }
//                });

                try { Thread.sleep(time); }catch (Exception ee){ }
            }

            txtMessageProgress = "Тестирование завершено";
            runH(txtMessageProgress, false);
            try { Thread.sleep(5000); }catch (Exception ee){ }

            stopH();
            return null;
        }
    }


    private void printPFD() {
        try {

            startProgressDialog("Печать: ПФД", false);

            Runnable runnable = new Runnable() {
                public void run() {


                    int er = tmAZ.openFiscalDocEx(0, 0, 0, 1, 1, 1, 40,  1, 24, "101");
                    //er = tmAZ.pdfPaint(2, 7, 0, "Test Test Test Test Test Test Test Test Test Test Test Test Test");
                    //String pdf417= "001184659884768011227880680296462533 acVv7BSqiytEeSiBSw9SQQgqwR2 / UdGW0aWphtUH61+cpJ0GIotByeN1noJgIrxprtmIJyKFmR8=\\ \\ À - ХА-ХА-ХА-ХА-ХА-ХА-ХА!Ля.\\1\\1211\\0822 \\ Ñ1 \\ Ë2422 \\ Â7053 \\ Ê2 \\ Â4\\M22";

                    //String pdf417="Test Test Test Test Test Test Test Test Test Test Test Test Test 1111 1111 111111 111111111111111111";
                   // er = tmAZ.pdfPaint(8, 7, 0, pdf417);

//                    er = tmAZ.addPosField(
//                            1, 1, 0,
//                            2, 1, 0,
//                            3, 1, 0,
//                            4, 1, 0,
//                            15, 1, 0,
//                            14, 1, 0,
//                            15, 1, 0);

                    tmAZ.addSerPosField(1, 1, 0);
                    tmAZ.addDocPosField( 2, 1, 0);
                    tmAZ.addDatePosField(3, 1, 0);
                    tmAZ.addTimePosField(4, 1, 0);
                    tmAZ.addINNPosField(15, 1, 0);
                    tmAZ.addOperPosField(14, 1, 0);
                    tmAZ.addSumPosField(15, 1, 0);

                    for (int i = 4; i < 12; i++) {
                        er = tmAZ.addFreeField((i), (1), 1, 1, 0, "NUM: " + Integer.toString(i + 1));
                    }
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<1030>5555|10.1|1|01010101|");
//                    er = tmAZ.addFreeField(1, 17, 0, 1, 0, "<1038>");
//                    er = tmAZ.addFreeField(1, 18, 0, 1, 0, "<1042>");
//                    er = tmAZ.addFreeField(1, 19, 0, 1, 0, "<1055>01");


                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<1030>5555|10.1|1|01010101|");
                    er = tmAZ.addFreeField(1, 17, 0, 1, 0, "<1038>");
                    er = tmAZ.addFreeField(1, 18, 0, 1, 0, "<1042>");
                    er = tmAZ.addFreeField(1, 19, 0, 1, 0, "<1055>01");



//                    er = tmAZ.addFreeField(1, 1, 0, 1, 0, "<105901>1234567890|30|1|0100040440|");
//                    er = tmAZ.addFreeField(1, 1, 0, 1, 0, "<1038>");
//                    er = tmAZ.addFreeField(1, 18, 0, 1, 0, "<1042>");
//                    er = tmAZ.addFreeField(1, 19, 0, 1, 0, "<1055>00");
//
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<107901>");
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<102301>");
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<104301>");
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<119901>");
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<120001>");
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<122201>");
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<117101>117101");



                            er = tmAZ.addFreeField(1,14,0,1,0,"<1009>123");
                            er = tmAZ.addFreeField(1,14,0,1,0,"<1187>123");
                    //er = tmAZ.addFreeField(0,0,0,2,0,"");
                    //er = tmAZ.addFreeField(1, 1, 0, 1, 30, "J01H00f00h02w02k4aT0");
                    // "j16h30H00w02f31J01R00k49T7B43000B6207564110380B"

                    er = tmAZ.addFreeField(1, 1, 1, 1, 30, "j16h30H00w02f31J01R00k49T313233343536373839302D");
                    er = tmAZ.addFreeField(1, 1, 1, 1, 30, "j16h30H00w02f31J01R00k49T7B43000B6207564110380B");
                    er = tmAZ.addFreeField(1, 1, 0, 1, 30, "J01H00f00h03w03k4aT0");

                    er = tmAZ.printFiscalReceipt();

                    //int er = printFRCommandPFD();

                    String err = TMError.GetText((byte)er);

                    runH("printFiscalReceipt: " + Integer.toString(er) + " " + err, false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    runH("Ответ: " + libTM.getLastResCMD(), false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                    stopH();

                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
        }
        catch (Exception e) {
            Toast.makeText(this, "Exception pressPFD: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void pressPFD(View b) {
        printPFD();
    }

    private int printFRCommandPFD() {
        try {

            String pdf417= "001184659884768011227880680296462533 acVv7BSqiytEeSiBSw9SQQgqwR2 / UdGW0aWphtUH61+cpJ0GIotByeN1noJgIrxprtmIJyKFmR8=\\ \\ À - ХА-ХА-ХА-ХА-ХА-ХА-ХА!Ля.\\1\\1211\\0822 \\ Ñ1 \\ Ë2422 \\ Â7053 \\ Ê2 \\ Â4\\M22";

            //String pdf417="Test Test Test Test Test Test Test Test Test Test Test Test Test 1111 1111 111111 111111111111111111";
            int er = tmAZ.pdfPaint(8, 7, 0, pdf417);
            //int er = tmAZ.pdfPaint(6, 6, 500, "TestPDF417");
//            int[] cmd_int = new int[]{  0x02, 0x41, 0x45, 0x52, 0x46, 0x31, 0x37, 0x33, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x30, 0x30, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x31, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x44, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x38, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x31, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x32, 0x30, 0x30, 0x1C, 0x32, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x31, 0x37, 0x30, 0x30, 0x1C, 0x30, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x33, 0x30, 0x30, 0x1C, 0x30, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0xC6, 0xE0, 0xF0, 0xE8, 0xEA, 0xEE, 0xE2, 0xE0, 0x20, 0x20, 0xCC, 0x2E, 0xC1, 0x2E, 0x1C, 0x30, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x45, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x31, 0x37, 0x36, 0x2E, 0x36, 0x34, 0x1C, 0x31, 0x42, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD1, 0xE5, 0xF0, 0xE8, 0xE9, 0xED, 0xFB, 0xE9, 0x20, 0x4E, 0x3A, 0x1C, 0x30, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x4E, 0x20, 0xE4, 0xEE, 0xEA, 0x3A, 0x1C, 0x31, 0x37, 0x30, 0x30, 0x1C, 0x30, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xC8, 0xCD, 0xCD, 0x3A, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x35, 0x35, 0x3E, 0x30, 0x30, 0x1C, 0x30, 0x33, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xCA, 0xC0, 0xD1, 0xD1, 0xC8, 0xD0, 0x3A, 0x1C, 0x30, 0x34, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xCC, 0xE8, 0xED, 0xE5, 0xF0, 0xE0, 0xEB, 0xFC, 0xED, 0xFB, 0xE5, 0x20, 0xC2, 0xEE, 0xE4, 0xFB, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0xCC, 0xCA, 0xC0, 0xD1, 0xD1, 0xC0, 0x3A, 0x36, 0x36, 0x36, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD7, 0xC5, 0xCA, 0x3A, 0x20, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x30, 0x36, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x34, 0x32, 0x3E, 0x31, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x31, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD1, 0xCC, 0xC5, 0xCD, 0xC0, 0x3A, 0x20, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x32, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x33, 0x38, 0x3E, 0x31, 0x1C, 0x30, 0x36, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xCF, 0xD0, 0xC8, 0xC3, 0x2E, 0x20, 0xCF, 0xCE, 0xC5, 0xC7, 0xC4, 0x1C, 0x30, 0x37, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xED, 0xE0, 0x20, 0x32, 0x34, 0x2D, 0x30, 0x39, 0x2D, 0x32, 0x30, 0x31, 0x39, 0x2D, 0x3E, 0xCF, 0x1C, 0x30, 0x38, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xEE, 0xF2, 0x20, 0xCC, 0xC8, 0xCD, 0xC5, 0xD0, 0xC0, 0xCB, 0xDC, 0xCD, 0xDB, 0xC5, 0x20, 0xC2, 0xCE, 0xC4, 0xDB, 0x1C, 0x30, 0x39, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xE4, 0xEE, 0x20, 0xCA, 0xC8, 0xD1, 0xCB, 0xCE, 0xC2, 0xCE, 0xC4, 0xD1, 0xCA, 0x1C, 0x30, 0x41, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xC1, 0xE8, 0xEB, 0xE5, 0xF2, 0x20, 0x4E, 0x3A, 0x20, 0x30, 0x30, 0x30, 0x33, 0x39, 0x20, 0x20, 0x20, 0x20, 0x20, 0xD1, 0xE8, 0xF1, 0xF2, 0x2E, 0x4E, 0x3A, 0x20, 0x30, 0x36, 0x36, 0x36, 0x30, 0x30, 0x30, 0x31, 0x37, 0x35, 0x39, 0x37, 0x30, 0x1C, 0x30, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xEE, 0xF2, 0x20, 0xCC, 0xC8, 0xCD, 0xC5, 0xD0, 0xC0, 0xCB, 0xDC, 0xCD, 0xDB, 0xC5, 0x20, 0xC2, 0xCE, 0xC4, 0xDB, 0x1C, 0x30, 0x43, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xE4, 0xEE, 0x20, 0xCA, 0xC8, 0xD1, 0xCB, 0xCE, 0xC2, 0xCE, 0xC4, 0xD1, 0xCA, 0x1C, 0x30, 0x44, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x33, 0x30, 0x3E, 0xCF, 0xE5, 0xF0, 0xE5, 0xE2, 0xEE, 0xE7, 0xEA, 0xE0, 0x20, 0xD0, 0xE0, 0xE7, 0xEE, 0xE2, 0xFB, 0xE9, 0x20, 0xCF, 0xEE, 0xEB, 0xED, 0xFB, 0xE9, 0x20, 0x2D, 0x3E, 0x20, 0xCF, 0x7C, 0x31, 0x37, 0x36, 0x2E, 0x36, 0x34, 0x7C, 0x31, 0x7C, 0x30, 0x31, 0x30, 0x31, 0x30, 0x34, 0x30, 0x34, 0x7C, 0x1C, 0x30, 0x45, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD1, 0xF2, 0xEE, 0xE8, 0xEC, 0xEE, 0xF1, 0xF2, 0xFC, 0x20, 0xEF, 0xEE, 0x20, 0xF2, 0xE0, 0xF0, 0xE8, 0xF4, 0xF3, 0x3A, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x3D, 0x31, 0x37, 0x36, 0x2E, 0x36, 0x34, 0x1C, 0x30, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xC8, 0xD2, 0xCE, 0xC3, 0x1C, 0x31, 0x30, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0xD1, 0xCE, 0xD5, 0xD0, 0xC0, 0xCD, 0xDF, 0xC9, 0xD2, 0xC5, 0x20, 0xC1, 0xC8, 0xCB, 0xC5, 0xD2, 0x1C, 0x31, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0xC4, 0xCE, 0x20, 0xCA, 0xCE, 0xCD, 0xD6, 0xC0, 0x20, 0xCF, 0xCE, 0xC5, 0xC7, 0xC4, 0xCA, 0xC8, 0x1C, 0x31, 0x33, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0x20, 0x20, 0xF2, 0xE5, 0xEB, 0x2E, 0x20, 0x38, 0x2D, 0x38, 0x30, 0x30, 0x2D, 0x37, 0x37, 0x35, 0x2D, 0x31, 0x35, 0x2D, 0x30, 0x37, 0x20, 0x28, 0xE1, 0xE5, 0xF1, 0xEF, 0xEB, 0xE0, 0xF2, 0xED, 0xEE, 0x29, 0x1C, 0x31, 0x35, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0x20, 0x20, 0xCF, 0xF0, 0xE8, 0xEE, 0xE1, 0xF0, 0xE5, 0xF2, 0xE0, 0xE9, 0xF2, 0xE5, 0x20, 0xE1, 0xE8, 0xEB, 0xE5, 0xF2, 0xFB, 0x20, 0xEE, 0xED, 0xEB, 0xE0, 0xE9, 0xED, 0x20, 0xF7, 0xE5, 0xF0, 0xE5, 0xE7, 0x1C, 0x31, 0x36, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0x20, 0x20, 0x20, 0xEC, 0xEE, 0xE1, 0xE8, 0xEB, 0xFC, 0xED, 0xEE, 0xE5, 0x20, 0xEF, 0xF0, 0xE8, 0xEB, 0xEE, 0xE6, 0xE5, 0xED, 0xE8, 0xE5, 0x20, 0xC0, 0xCE, 0x20, 0x22, 0xD0, 0xC6, 0xC4, 0x22, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x31, 0x43, 0x1C, 0x6A, 0x31, 0x36, 0x68, 0x33, 0x30, 0x48, 0x30, 0x30, 0x77, 0x30, 0x32, 0x66, 0x33, 0x31, 0x4A, 0x30, 0x31, 0x52, 0x30, 0x30, 0x6B, 0x34, 0x39, 0x54, 0x37, 0x42, 0x34, 0x33, 0x30, 0x30, 0x30, 0x42, 0x36, 0x32, 0x30, 0x37, 0x35, 0x36, 0x34, 0x31, 0x31, 0x30, 0x33, 0x38, 0x30, 0x42, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x31, 0x44, 0x1C, 0x6A, 0x31, 0x36, 0x68, 0x33, 0x30, 0x48, 0x30, 0x30, 0x77, 0x30, 0x32, 0x66, 0x33, 0x31, 0x4A, 0x30, 0x31, 0x52, 0x30, 0x30, 0x6B, 0x34, 0x39, 0x54, 0x37, 0x42, 0x34, 0x33, 0x31, 0x36, 0x35, 0x44, 0x31, 0x41, 0x31, 0x32, 0x30, 0x32, 0x32, 0x41, 0x31, 0x38, 0x34, 0x41, 0x32, 0x31, 0x1C, 0x03, 0x36, 0x41, 0x39, 0x42}; // read_file_command();
//
//            for(int i = 0; i < cmd_int.length; i++) {
//                cmd[i] = (byte) cmd_int[i];
//            }

            byte[] cmd = read_file_command(checkBoxPDF417);
            //byte[] cmd = new byte[cmd_int.length];


            int i_cmd  = 0;
            TMCommand cmd_ = new TMCommand();
            if (cmd_.SetPacket(cmd) > 0) {
                i_cmd = libTM.DoCmd(cmd_, 3000);
            }
            else {
                i_cmd = libTM.DoCmd(cmd, 3000);
            }

            return i_cmd;

//            String err = TMError.GetText((byte)res);
//            Toast.makeText(this, "shiftClose: " + Integer.toString(res) + " " + err, Toast.LENGTH_LONG).show();

            //Toast.makeText(this, "Ответ: " + libTM.getLastResCMD(), Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            Toast.makeText(this, "Exception pressCloseShift: " + e.getMessage(), Toast.LENGTH_LONG).show();
            return 999;
        }
    }


    public void pressPrintGray(View w) {
        //Получаем вид с файла dialog_print_gray.xml, который применим для диалогового окна:
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_print_gray, null);

        //Создаем AlertDialog
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);

        //Настраиваем prompt.xml для нашего AlertDialog:
        mDialogBuilder.setView(promptsView);

        //Настраиваем отображение поля для ввода текста в открытом диалоге:
        final EditText grayInput = (EditText)promptsView.findViewById(R.id.input_gray);


        //Настраиваем сообщение в диалоговом окне:
        mDialogBuilder
                .setCancelable(false)
                .setTitle("PRINTER GRAY")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                int gray_print = Integer.valueOf(String.valueOf(grayInput.getText()));
                                libTM.grayInt = gray_print;
                            }
                        })
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        //Создаем AlertDialog:
        AlertDialog alertDialog = mDialogBuilder.create();

        //и отображаем его:
        alertDialog.show();
    }

    public void pressPrintTest(View b) {
        //Получаем вид с файла prompt.xml, который применим для диалогового окна:
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_print_test, null);

        //Создаем AlertDialog
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);

        //Настраиваем prompt.xml для нашего AlertDialog:
        mDialogBuilder.setView(promptsView);

        //Настраиваем отображение поля для ввода текста в открытом диалоге:
        final EditText tryInput = (EditText)promptsView.findViewById(R.id.input_try);
        final EditText timeInput = (EditText)promptsView.findViewById(R.id.input_timeout);
        final CheckBox checkBoxPDF = (CheckBox)promptsView.findViewById(R.id.checkBoxPDF);
        currentDateTime = (TextView)promptsView.findViewById(R.id.text_currentDateTime);
        setInitialDateTime(currentDateTime);

        // ======================================
        // ======================================
        //        boolean b;
        //        if (date1.getTime() > date2.getTime())
        //            b = true;
        //        else b = false;
        //        System.out.println(b);
        String myStrDate = currentDateTime.getText().toString();;// "11/08/2013 08:48:10"; // 14 ноября 2019 г., 13:20
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date date = format.parse(myStrDate);

            String myStrDate1 = "12/08/2013 08:48:11";
            String myStrDate2 = "12/08/2013 08:48:12";

            boolean bh = false;
            Date date1 = format.parse(myStrDate1);;
            Date date2 =  format.parse(myStrDate2);;
            if (date1.getTime() > date2.getTime())
            {
                bh = true;
            }else {
                bh = false;
            }

            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // ======================================
        // ======================================

        //Настраиваем сообщение в диалоговом окне:
        mDialogBuilder
                .setCancelable(false)
                .setTitle("PRINTER TEST")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                Integer params[] = new Integer[2];
                                params[0] = Integer.valueOf(String.valueOf(tryInput.getText()));
                                params[1] = Integer.valueOf(String.valueOf(timeInput.getText()));
                                data_limit = currentDateTime.getText().toString();
                                checkBoxPDF417 = checkBoxPDF.isChecked();

                                PrintTest test = new PrintTest();
                                test.execute(params);
//                                //составная операция, сперва получаем время из ФР
//                                cmdNum = TMCommand.commands.CM_ToCash;
//                                FRGetDateTime();
                            }
                        })
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        //Создаем AlertDialog:
        AlertDialog alertDialog = mDialogBuilder.create();

        //и отображаем его:
        alertDialog.show();

    }

    // установка обработчика выбора времени
    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTime.set(Calendar.MINUTE, minute);
            setInitialDateTime(currentDateTime);
        }
    };

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime(currentDateTime);
        }
    };

    // отображаем диалоговое окно для выбора даты
    public void setDate(View v) {
        new DatePickerDialog(MainActivity.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // отображаем диалоговое окно для выбора времени
    public void setTime(View v) {
        new TimePickerDialog(MainActivity.this, t,
                dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true)
                .show();
    }

    public void sendOFD(View b) {

//        tmAZ.startFSService(true);
////        try {
////            Thread.sleep(2000);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        }
//        for(int i = 0; i < 30; i ++) {
//            int ii = tmAZ.getEnvelopesCount();
//            Log.i("sendOFD", "Сообщенией: "+ ii);
//            if(ii == 0) {
//                break;
//            }
//            Log.i("sendOFD", "Сообщенией: "+ ii);
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//

        tmAZ.stopFSService();


        sendOFD();
    }

    public void printLastDoc(View b) {
        printLastDoc();
    }

    byte[] fullyReadFileToBytes(File f) throws IOException {
        int size = (int)f.length();
        byte[] bytes = new byte[size];
        byte[] tmpBuff = new byte[size];
        FileInputStream fis = new FileInputStream(f);

        try {
            int read = fis.read(bytes, 0, size);
            if (read < size) {
                for(int remain = size - read; remain > 0; remain -= read) {
                    read = fis.read(tmpBuff, 0, remain);
                    System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
                }
            }
        } catch (IOException var11) {
            throw var11;
        } finally {
            fis.close();
        }

        return bytes;
    }
    private byte[] read_file_command(boolean pdf417OrLine) {
        byte[] res_b = new byte[0];

        try {
            String path_file = "";
            if(!pdf417OrLine == true) {
                path_file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/cmdFR.bin";
            }else {
                path_file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/cmdFR.bin";
            }
            File file = new File(path_file);
            if ( file.exists() ) {
                res_b = this.fullyReadFileToBytes(file);
            }
            else {
                //default cmd\
                InputStream is;
                if(!pdf417OrLine == true) {
                    is = getResources().openRawResource(R.raw.cmdfr2);
                }else {
                    is = getResources().openRawResource(R.raw.cmdfr2);
                }

                is = getResources().openRawResource(R.raw.cmdfr5);

                int size = is.available();
                res_b = new byte[size];
                is.read(res_b);
            }
            return res_b;
        } catch (Exception var3) {
            String xtx = var3.getMessage();
            return res_b;
        }
    }

    public void printFRCommand(View b) {
        printFRCommand();
    }


    private void printTTTest()
    {
        try {
            int er = tmAZ.openFiscalDocEx(0, 0, 0, 1, 1, 1, 40,  1, 24, "101");
            er = tmAZ.pdfPaint(2, 7, 0, "Test Test Test Test Test Test Test Test Test Test Test Test Test");
            //String pdf417= "001184659884768011227880680296462533 acVv7BSqiytEeSiBSw9SQQgqwR2 / UdGW0aWphtUH61+cpJ0GIotByeN1noJgIrxprtmIJyKFmR8=\\ \\ À - ХА-ХА-ХА-ХА-ХА-ХА-ХА!Ля.\\1\\1211\\0822 \\ Ñ1 \\ Ë2422 \\ Â7053 \\ Ê2 \\ Â4\\M22";

            //String pdf417="Test Test Test Test Test Test Test Test Test Test Test Test Test 1111 1111 111111 111111111111111111";
            // er = tmAZ.pdfPaint(8, 7, 0, pdf417);

//                    er = tmAZ.addPosField(
//                            1, 1, 0,
//                            2, 1, 0,
//                            3, 1, 0,
//                            4, 1, 0,
//                            15, 1, 0,
//                            14, 1, 0,
//                            15, 1, 0);

            tmAZ.addSerPosField(1, 1, 0);
            tmAZ.addDocPosField( 2, 1, 0);
            tmAZ.addDatePosField(3, 1, 0);
            tmAZ.addTimePosField(4, 1, 0);
            tmAZ.addINNPosField(15, 1, 0);
            tmAZ.addOperPosField(14, 1, 0);
            tmAZ.addSumPosField(15, 1, 0);

            for (int i = 0; i < 10; i++) {
                er = tmAZ.addFreeField((i + 1), (3), 1, 1, 0, "Строка: " + Integer.toString(i + 1));
            }
//                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<1030>5555|10.1|1|01010101|");
//                    er = tmAZ.addFreeField(1, 17, 0, 1, 0, "<1038>");
//                    er = tmAZ.addFreeField(1, 18, 0, 1, 0, "<1042>");
//                    er = tmAZ.addFreeField(1, 19, 0, 1, 0, "<1055>01");


                    er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<1030>5555|10.1|1|01010101|");
                    er = tmAZ.addFreeField(1, 17, 0, 1, 0, "<1038>");
                    er = tmAZ.addFreeField(1, 18, 0, 1, 0, "<1042>");
                    er = tmAZ.addFreeField(1, 19, 0, 1, 0, "<1055>01");



//            er = tmAZ.addFreeField(1, 1, 0, 1, 0, "<105901>1234567890|30|1|0100040440|");
//            er = tmAZ.addFreeField(1, 1, 0, 1, 0, "<1038>");
//            er = tmAZ.addFreeField(1, 18, 0, 1, 0, "<1042>");
//            er = tmAZ.addFreeField(1, 19, 0, 1, 0, "<1055>00");
//
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<107901>");
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<102301>");
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<104301>");
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<119901>");
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<120001>");
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<122201>");
//            er = tmAZ.addFreeField(1, 16, 0, 1, 0, "<117101>117101");



                    er = tmAZ.addFreeField(1,14,0,1,0,"<1009>123");
                    er = tmAZ.addFreeField(1,14,0,1,0,"<1187>123");
            //er = tmAZ.addFreeField(0,0,0,2,0,"");
            er = tmAZ.addFreeField(1, 1, 0, 1, 30, "J01H00f00h02w02k4aT0");
            // "j16h30H00w02f31J01R00k49T7B43000B6207564110380B"

                    er = tmAZ.addFreeField(1, 1, 1, 1, 30, "j16h30H00w02f31J01R00k49T313233343536373839302D");
                    //er = tmAZ.addFreeField(1, 1, 1, 1, 30, "j16h30H00w02f31J01R00k49T7B43000B6207564110380B");
            //er = tmAZ.addFreeField(1, 1, 0, 1, 30, "J01H00f00h03w03k4aT0");

            er = tmAZ.printFiscalReceipt();
        }catch (Exception e) {
            Toast.makeText(this, "Exception pressCloseShift: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void printFRCommand() {
        try {
            //String pdf417= "001184659884768011227880680296462533 acVv7BSqiytEeSiBSw9SQQgqwR2 / UdGW0aWphtUH61+cpJ0GIotByeN1noJgIrxprtmIJyKFmR8=\\ \\ À - ХА-ХА-ХА-ХА-ХА-ХА-ХА!Ля.\\1\\1211\\0822 \\ Ñ1 \\ Ë2422 \\ Â7053 \\ Ê2 \\ Â4\\M22";
            //int er = tmAZ.pdfPaint(8, 7, 0, pdf417);
//            String pdf417="Test Test Test Test Test Test Test Test Test Test Test Test Test 1111 1111 111111 111111111111111111";
//            int er = tmAZ.pdfPaint(8, 7, 0, pdf417);
           //int er = tmAZ.pdfPaint(6, 6, 500, "TestPDF417");
//            int[] cmd_int = new int[]{  0x02, 0x41, 0x45, 0x52, 0x46, 0x31, 0x37, 0x33, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x30, 0x30, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x31, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x44, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x38, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x31, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x32, 0x30, 0x30, 0x1C, 0x32, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x31, 0x37, 0x30, 0x30, 0x1C, 0x30, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x33, 0x30, 0x30, 0x1C, 0x30, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0xC6, 0xE0, 0xF0, 0xE8, 0xEA, 0xEE, 0xE2, 0xE0, 0x20, 0x20, 0xCC, 0x2E, 0xC1, 0x2E, 0x1C, 0x30, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x45, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x31, 0x37, 0x36, 0x2E, 0x36, 0x34, 0x1C, 0x31, 0x42, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD1, 0xE5, 0xF0, 0xE8, 0xE9, 0xED, 0xFB, 0xE9, 0x20, 0x4E, 0x3A, 0x1C, 0x30, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x4E, 0x20, 0xE4, 0xEE, 0xEA, 0x3A, 0x1C, 0x31, 0x37, 0x30, 0x30, 0x1C, 0x30, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xC8, 0xCD, 0xCD, 0x3A, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x35, 0x35, 0x3E, 0x30, 0x30, 0x1C, 0x30, 0x33, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xCA, 0xC0, 0xD1, 0xD1, 0xC8, 0xD0, 0x3A, 0x1C, 0x30, 0x34, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xCC, 0xE8, 0xED, 0xE5, 0xF0, 0xE0, 0xEB, 0xFC, 0xED, 0xFB, 0xE5, 0x20, 0xC2, 0xEE, 0xE4, 0xFB, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0xCC, 0xCA, 0xC0, 0xD1, 0xD1, 0xC0, 0x3A, 0x36, 0x36, 0x36, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD7, 0xC5, 0xCA, 0x3A, 0x20, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x30, 0x36, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x34, 0x32, 0x3E, 0x31, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x31, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD1, 0xCC, 0xC5, 0xCD, 0xC0, 0x3A, 0x20, 0x1C, 0x30, 0x35, 0x30, 0x30, 0x1C, 0x32, 0x32, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x33, 0x38, 0x3E, 0x31, 0x1C, 0x30, 0x36, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xCF, 0xD0, 0xC8, 0xC3, 0x2E, 0x20, 0xCF, 0xCE, 0xC5, 0xC7, 0xC4, 0x1C, 0x30, 0x37, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xED, 0xE0, 0x20, 0x32, 0x34, 0x2D, 0x30, 0x39, 0x2D, 0x32, 0x30, 0x31, 0x39, 0x2D, 0x3E, 0xCF, 0x1C, 0x30, 0x38, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xEE, 0xF2, 0x20, 0xCC, 0xC8, 0xCD, 0xC5, 0xD0, 0xC0, 0xCB, 0xDC, 0xCD, 0xDB, 0xC5, 0x20, 0xC2, 0xCE, 0xC4, 0xDB, 0x1C, 0x30, 0x39, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xE4, 0xEE, 0x20, 0xCA, 0xC8, 0xD1, 0xCB, 0xCE, 0xC2, 0xCE, 0xC4, 0xD1, 0xCA, 0x1C, 0x30, 0x41, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xC1, 0xE8, 0xEB, 0xE5, 0xF2, 0x20, 0x4E, 0x3A, 0x20, 0x30, 0x30, 0x30, 0x33, 0x39, 0x20, 0x20, 0x20, 0x20, 0x20, 0xD1, 0xE8, 0xF1, 0xF2, 0x2E, 0x4E, 0x3A, 0x20, 0x30, 0x36, 0x36, 0x36, 0x30, 0x30, 0x30, 0x31, 0x37, 0x35, 0x39, 0x37, 0x30, 0x1C, 0x30, 0x42, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xEE, 0xF2, 0x20, 0xCC, 0xC8, 0xCD, 0xC5, 0xD0, 0xC0, 0xCB, 0xDC, 0xCD, 0xDB, 0xC5, 0x20, 0xC2, 0xCE, 0xC4, 0xDB, 0x1C, 0x30, 0x43, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xE4, 0xEE, 0x20, 0xCA, 0xC8, 0xD1, 0xCB, 0xCE, 0xC2, 0xCE, 0xC4, 0xD1, 0xCA, 0x1C, 0x30, 0x44, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x3C, 0x31, 0x30, 0x33, 0x30, 0x3E, 0xCF, 0xE5, 0xF0, 0xE5, 0xE2, 0xEE, 0xE7, 0xEA, 0xE0, 0x20, 0xD0, 0xE0, 0xE7, 0xEE, 0xE2, 0xFB, 0xE9, 0x20, 0xCF, 0xEE, 0xEB, 0xED, 0xFB, 0xE9, 0x20, 0x2D, 0x3E, 0x20, 0xCF, 0x7C, 0x31, 0x37, 0x36, 0x2E, 0x36, 0x34, 0x7C, 0x31, 0x7C, 0x30, 0x31, 0x30, 0x31, 0x30, 0x34, 0x30, 0x34, 0x7C, 0x1C, 0x30, 0x45, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xD1, 0xF2, 0xEE, 0xE8, 0xEC, 0xEE, 0xF1, 0xF2, 0xFC, 0x20, 0xEF, 0xEE, 0x20, 0xF2, 0xE0, 0xF0, 0xE8, 0xF4, 0xF3, 0x3A, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x3D, 0x31, 0x37, 0x36, 0x2E, 0x36, 0x34, 0x1C, 0x30, 0x46, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0xC8, 0xD2, 0xCE, 0xC3, 0x1C, 0x31, 0x30, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0xD1, 0xCE, 0xD5, 0xD0, 0xC0, 0xCD, 0xDF, 0xC9, 0xD2, 0xC5, 0x20, 0xC1, 0xC8, 0xCB, 0xC5, 0xD2, 0x1C, 0x31, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x32, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0xC4, 0xCE, 0x20, 0xCA, 0xCE, 0xCD, 0xD6, 0xC0, 0x20, 0xCF, 0xCE, 0xC5, 0xC7, 0xC4, 0xCA, 0xC8, 0x1C, 0x31, 0x33, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0x20, 0x20, 0xF2, 0xE5, 0xEB, 0x2E, 0x20, 0x38, 0x2D, 0x38, 0x30, 0x30, 0x2D, 0x37, 0x37, 0x35, 0x2D, 0x31, 0x35, 0x2D, 0x30, 0x37, 0x20, 0x28, 0xE1, 0xE5, 0xF1, 0xEF, 0xEB, 0xE0, 0xF2, 0xED, 0xEE, 0x29, 0x1C, 0x31, 0x35, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0x20, 0x20, 0xCF, 0xF0, 0xE8, 0xEE, 0xE1, 0xF0, 0xE5, 0xF2, 0xE0, 0xE9, 0xF2, 0xE5, 0x20, 0xE1, 0xE8, 0xEB, 0xE5, 0xF2, 0xFB, 0x20, 0xEE, 0xED, 0xEB, 0xE0, 0xE9, 0xED, 0x20, 0xF7, 0xE5, 0xF0, 0xE5, 0xE7, 0x1C, 0x31, 0x36, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x30, 0x1C, 0x20, 0x20, 0x20, 0x20, 0x20, 0xEC, 0xEE, 0xE1, 0xE8, 0xEB, 0xFC, 0xED, 0xEE, 0xE5, 0x20, 0xEF, 0xF0, 0xE8, 0xEB, 0xEE, 0xE6, 0xE5, 0xED, 0xE8, 0xE5, 0x20, 0xC0, 0xCE, 0x20, 0x22, 0xD0, 0xC6, 0xC4, 0x22, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x31, 0x43, 0x1C, 0x6A, 0x31, 0x36, 0x68, 0x33, 0x30, 0x48, 0x30, 0x30, 0x77, 0x30, 0x32, 0x66, 0x33, 0x31, 0x4A, 0x30, 0x31, 0x52, 0x30, 0x30, 0x6B, 0x34, 0x39, 0x54, 0x37, 0x42, 0x34, 0x33, 0x30, 0x30, 0x30, 0x42, 0x36, 0x32, 0x30, 0x37, 0x35, 0x36, 0x34, 0x31, 0x31, 0x30, 0x33, 0x38, 0x30, 0x42, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x30, 0x30, 0x1C, 0x30, 0x31, 0x1C, 0x30, 0x31, 0x1C, 0x31, 0x44, 0x1C, 0x6A, 0x31, 0x36, 0x68, 0x33, 0x30, 0x48, 0x30, 0x30, 0x77, 0x30, 0x32, 0x66, 0x33, 0x31, 0x4A, 0x30, 0x31, 0x52, 0x30, 0x30, 0x6B, 0x34, 0x39, 0x54, 0x37, 0x42, 0x34, 0x33, 0x31, 0x36, 0x35, 0x44, 0x31, 0x41, 0x31, 0x32, 0x30, 0x32, 0x32, 0x41, 0x31, 0x38, 0x34, 0x41, 0x32, 0x31, 0x1C, 0x03, 0x36, 0x41, 0x39, 0x42}; // read_file_command();
//
//            for(int i = 0; i < cmd_int.length; i++) {
//                cmd[i] = (byte) cmd_int[i];
//            }

            byte[] cmd = read_file_command(checkBoxPDF417);
            //byte[] cmd = new byte[cmd_int.length];

            TMCommand cmd_ = new TMCommand();
            if (cmd_.SetPacket(cmd) > 0) {
                int i_cmd = libTM.DoCmd(cmd_, 3000);
                int b = i_cmd;
            }
            else {
                int i_cmd = libTM.DoCmd(cmd, 3000);
            }


//            String err = TMError.GetText((byte)res);
//            Toast.makeText(this, "shiftClose: " + Integer.toString(res) + " " + err, Toast.LENGTH_LONG).show();

            //Toast.makeText(this, "Ответ: " + libTM.getLastResCMD(), Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            Toast.makeText(this, "Exception pressCloseShift: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public void StatusFR(View b) {
        StatusFR();
    }


    private String readFromFile(String nameFile) {
        String aBuffer = "";
        try {
            File myFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)  + "/" + nameFile);
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));
            String aDataRow = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += "\n" + aDataRow;
            }
            myReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aBuffer;
    }

    private void printOEM() {
        startProgressDialog("Печать произвольного нефискального текста", false);

        Runnable runnable = new Runnable() {
            public void run() {

                int res = 0;
                //tmAZ.printOEMDoc("Строка для печати 123123", 0 );

                tmAZ.openFDoc();

                String txt = readFromFile("text.txt");
                if(txt.length() > 0) {
                    res = tmAZ.printOEMDoc(txt.toString(), 0);
                } else {
                    for(int i = 0; i < 2; i ++) {
                        txt = "Утром мы во двор идем -\n" +
                                "Листья сыплются дождём,\n" +
                                "Под ногами шелестят\n" +
                                "И летят… летят… летят…\n" +
                                "\n" +
                                "Пролетают паутинки\n" +
                                "С паучками в серединке,\n" +
                                "И высоко от земли\n" +
                                "Пролетели журавли.\n" +
                                "\n" +
                                "Всё летит! Должно быть, это\n" +
                                "Улетает наше лето. \n \n \n \n";


                        res = tmAZ.printOEMDoc(txt.toString(), 0);
                    }
                }
                tmAZ.closeFDoc();

                try {
                    Thread.sleep(txt.length());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                stopH();

            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void PrintOEMDoc(View w) {
        int res = 0;
//
//        String txt = "";//readFromFile("text.txt");
//        if(txt.length() > 0) {
//            res = tmAZ.printOEMDoc(txt.toString(), 0);
//        }
//
//        for(int i = 0; i < 20; i ++) {
//            txt += "Утром мы во двор идем —\n" +
//                    "Листья сыплются дождём,\n" +
//                    "Под ногами шелестят\n" +
//                    "И летят… летят… летят…\n" +
//                    "\n" +
//                    "Пролетают паутинки\n" +
//                    "С паучками в серединке,\n" +
//                    "И высоко от земли\n" +
//                    "Пролетели журавли.\n" +
//                    "\n" +
//                    "Всё летит! Должно быть, это\n" +
//                    "Улетает наше лето. \n \n \n";
//
//
//            res = tmAZ.printOEMDoc(txt.toString(), 0);
//        }

        printOEM();

        Toast.makeText(this, " Нефискальный документ: " + res , Toast.LENGTH_LONG).show();
    }

    private void printLastDoc() {

        startProgressDialog("Печать последнего документа", false);
        Runnable runnable = new Runnable() {
            public void run() {

                int m = 1;
                if(item == "1")
                {
                    m =  1;
                }
                if(item == "5")
                {
                    m =  5;
                }
                if(item == "10")
                {
                    m =  10;
                }

                for(int i = 0; i < m; i++)
                {
                    //!** Печать последнего документа
                    libTM.printLastDoc();

                    try { Thread.sleep(500); }catch (Exception ee){ }
                }



                stopH();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    public byte[] hexStringToByteArray(String s) {
        try {

            int len = s.length();
            if(len>1) {
                byte[] data = new byte[len / 2];
                for (int i = 0 ; i < len ; i += 2) {
                    data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                            + Character.digit(s.charAt(i + 1), 16));
                }
                return data;
            }
            else

            {
                return  null;
            }
        }catch (Exception e)
        {
            throw e;
        }
    }

    private void StatusFR() {

        startProgressDialog("Статус ККТ", false);

        Runnable runnable = new Runnable() {
            public void run() {

                //!** Статус контроллера
//                TMCommand cmd = new TMCommand();
//                cmd.CmdGetInfo();
//                int res = libTM.DoCmd(cmd, 2000);

                int res = 0;
                String inf = "";

//                res = tmAZ.getStatus();
//                runH("Ответ getStatus: " +  bytesToHex(libTM.getLastResByte()), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }

//                res = tmAZ.getEnvelopesCount();
//                runH("Ответ getEnvelopesCount: " +  res, false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }


//                res = tmAZ.getFiscalNums();
//                runH("Ответ: getFiscalNums" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                if(res == 0) {
//                    //TMCommand cmd_r = libTM.getLastResCMD();
//                    String info = ""; //tmAZ.getFldStr(1 ) + "\n";
//                    info += tmAZ.getFldStr(1 ) + "\n";
//                    info += tmAZ.getFldStr(2 ) + "\n";
//                    info += tmAZ.getFldStr(3 ) + "\n";
//                    info += tmAZ.getFldStr(4 ) + "\n";
//                    info += tmAZ.getFldStr(5 ) + "\n";
//                    info += tmAZ.getFldStr(6 ) + "\n";
//                    info += tmAZ.getFldStr(7 ) + "\n";
//                    info += tmAZ.getFldStr(8 ) + "\n";
//                    info += tmAZ.getFldStr(9 ) + "\n";
//                    info += tmAZ.getFldStr(10 ) + "\n";
//                    info += tmAZ.getFldStr(11 ) + "\n";
//
////                    //!** старая возможность
////                    String info = cmd.GetFieldValue(1) + "\n";
////                    info += cmd_r.GetFieldValue(2) + "\n";
////                    info += cmd_r.GetFieldValue(3) + "\n";
////                    info += cmd_r.GetFieldValue(4) + "\n";
////                    info += cmd_r.GetFieldValue(5) + "\n";
////                    info += cmd_r.GetFieldValue(6) + "\n";
////                    info += cmd_r.GetFieldValue(7) + "\n";
////                    info += cmd_r.GetFieldValue(8) + "\n";
////                    info += cmd_r.GetFieldValue(9) + "\n";
////                    info += cmd_r.GetFieldValue(10) + "\n";
////                    info += cmd_r.GetFieldValue(11) + "\n";
//
//                    runH( "info: " + info, false);
//                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//                }


                if (libTM.ActivateNew() != 0)  {
                    runH( "Необходима Инициализация", false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                } else {
                    runH( "Инициализация Выполнена", false);
                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
                }




//                res = tmAZ.getEReport((byte)0x37, (byte)0);
//                runH("Ответ: getEReport" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//
//
//
//                res = tmAZ.fnGetStatus();
//                inf = tmAZ.getFldStr(8);
//                res =  tmAZ.getFldByte(8);
//
//                runH("Ответ: fnGetStatus" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getResource();
//                inf = tmAZ.getFldStr(8);
//                inf = tmAZ.getFldStr(9);
//
//                runH("Ответ: getResource" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getEReport((byte)0x37, (byte)0);
//
//                runH("Ответ: getEReport" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                 //tmAZ.getFldStr(1 ) + "\n";
//                inf = tmAZ.getFldStr(5 ) + "\n";
//
//                res = tmAZ.fnGetCommStatusEx().getEnvelopesCount();
//                res = tmAZ.fnGetCommStatus();
//                runH("Ответ: fnGetCommStatus" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getFiscalParam(1209);
//                inf = tmAZ.getFldStr(5 ) + "\n";
//                runH("Ответ: getFiscalParam" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                //res = tmAZ.checkHealth();
//
//                res = tmAZ.getStatus();
//
//
//                runH("Ответ getLastResByte: " +  bytesToHex(libTM.getLastResByte()), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//
//                res = tmAZ.getStatusNum(0);
//                runH("Ответ getStatusNum(0): " +  bytesToHex(new byte[]{(byte)res}), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getStatusNum(1);
//                runH("Ответ getStatusNum(1): " +  bytesToHex(new byte[]{(byte)res}), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getStatusNum(2);
//                runH("Ответ getStatusNum(2): " +  bytesToHex(new byte[]{(byte)res}), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getStatusNum(3);
//                runH("Ответ getStatusNum(3): " +  bytesToHex(new byte[]{(byte)res}), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getStatusNum(4);
//                runH("Ответ getStatusNum(4): " +  bytesToHex(new byte[]{(byte)res}), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                res = tmAZ.getStatusNum(5);
//                runH("Ответ getStatusNum(5): " +  bytesToHex(new byte[]{(byte)res}), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }

//                res = tmAZ.getFiscalNums();
//                runH("Ответ: getFiscalNums" + libTM.getLastResCMD(), false);
//                try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//
//                if(res == 0) {
//                    //TMCommand cmd_r = libTM.getLastResCMD();
//                    String info = ""; //tmAZ.getFldStr(1 ) + "\n";
//                    info += tmAZ.getFldStr(1 ) + "\n";
//                    info += tmAZ.getFldStr(2 ) + "\n";
//                    info += tmAZ.getFldStr(3 ) + "\n";
//                    info += tmAZ.getFldStr(4 ) + "\n";
//                    info += tmAZ.getFldStr(5 ) + "\n";
//                    info += tmAZ.getFldStr(6 ) + "\n";
//                    info += tmAZ.getFldStr(7 ) + "\n";
//                    info += tmAZ.getFldStr(8 ) + "\n";
//                    info += tmAZ.getFldStr(9 ) + "\n";
//                    info += tmAZ.getFldStr(10 ) + "\n";
//                    info += tmAZ.getFldStr(11 ) + "\n";
//
////                    //!** старая возможность
////                    String info = cmd.GetFieldValue(1) + "\n";
////                    info += cmd_r.GetFieldValue(2) + "\n";
////                    info += cmd_r.GetFieldValue(3) + "\n";
////                    info += cmd_r.GetFieldValue(4) + "\n";
////                    info += cmd_r.GetFieldValue(5) + "\n";
////                    info += cmd_r.GetFieldValue(6) + "\n";
////                    info += cmd_r.GetFieldValue(7) + "\n";
////                    info += cmd_r.GetFieldValue(8) + "\n";
////                    info += cmd_r.GetFieldValue(9) + "\n";
////                    info += cmd_r.GetFieldValue(10) + "\n";
////                    info += cmd_r.GetFieldValue(11) + "\n";
//
//                    runH( "info: " + info, false);
//                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//                }






//                //!** DEE0 и DLE9
//                byte[] dataDLE0 = {0x10, 0x30};
//                byte[] dataDLE9 = {0x10, 0x39};
//                int iDLE0 = libTM.DoCmd(dataDLE0, 1000);
//                int iDLE9 = libTM.DoCmd(dataDLE9, 1000);
//
//                if(((byte)iDLE0 & TMLib.states.INIT) == 0) {
//                    runH( "Необходима Инициализация", false);
//                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//                    //Toast.makeText(this, " Необходима Инициализация" , Toast.LENGTH_LONG).show();
//                }
//                else {
//                    runH( "Инициализация Выполнена", false);
//                    try { Thread.sleep(TIMEDIPLAY); }catch (Exception ee){ }
//                    //Toast.makeText(this, " Иинициализация Выполнена" , Toast.LENGTH_LONG).show();
//                }

                stopH();

            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void sendOFD() {

        // отправка в ОФД разрешена
        OFDsendCancel = false;
        startProgressDialog("Отправка в ОФД", true);

        Runnable runnable = new Runnable() {
            public void run() {

                TMCommand cmd = new TMCommand();
                cmd.CmdGetWaitDocs();
                libTM.DoCmd(cmd, 1000);

                TMCommand cmd_r;
                cmd_r = libTM.getLastResCMD();
                int waitDocs = '\uffff' & cmd_r.GetFieldHexW(5);

                ofd_text = "";
                ofd_text += "Сообщений для отправки: " + waitDocs;

                runH(ofd_text, true);

                while (OFDsendCancel == false) {

                    try {
                        byte[] ofd_send = tmAZ.fsReadBlockData();
                        byte[] ofd_read = new byte[]{};

                        if (ofd_send != null) {
                            if (ofd_send.length > 0) {
                                ofd_read = tmAZ.sendData(ofd_send, "F1test.taxcom.ru", 7778);
                            }
                        }

                        if (ofd_read != null) {
                            if (ofd_read.length > 0) {
                                byte[] shortAnswer = new byte[ofd_read.length - 30];
                                System.arraycopy(ofd_read, 30, shortAnswer, 0, ofd_read.length - 30);

                                tmAZ.fsWriteBlockData(shortAnswer);
                            }
                        }

                        cmd_r = libTM.getLastResCMD();

//                        ofd_text = "";
//                        ofd_text = "read FN len: " + ofd_send.length + "\n";
//                        ofd_text += "read OFD len: " + ofd_read.length + "\n";
//                        ofd_text += "write FN cmd: " + cmd_r + "\n";
//
//                        handler.post(new Runnable() {
//                            public void run() {
//                                Toast.makeText(cnt, ofd_text, Toast.LENGTH_LONG).show();
//                            }
//                        });


                        cmd = new TMCommand();
                        cmd.CmdGetWaitDocs();
                        libTM.DoCmd(cmd, 1000);

                        cmd_r = libTM.getLastResCMD();
                        waitDocs = '\uffff' & cmd_r.GetFieldHexW(5);

                        ofd_text = "";
                        ofd_text = "read FN len: " + ofd_send.length + "\n";
                        ofd_text += "read OFD len: " + ofd_read.length + "\n";
                        ofd_text += "write FN cmd: " + cmd_r + "\n";
                        ofd_text += "Сообщений для отправки: " + waitDocs;

                        runH(ofd_text, true);
                        try { Thread.sleep(1000); }catch (Exception ee){ }

                        if(waitDocs == 0) {
                            OFDsendCancel = true;
                        }

                    } catch (Exception e) {
                        int i = 0;
                    }
                }

                stopH();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();

    }


    public void pressFRCommand2(View v) {
        startProgressDialog("Печать...", false);

        Runnable runnable = new Runnable() {
            public void run() {

                printFRCommand();

                try { Thread.sleep(5000); }catch (Exception ee){ }

                stopH();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
}


